.. test_project documentation master file, created by
   sphinx-quickstart on Sun Jun 17 19:14:52 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to test_project's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


vm package
==========

Subpackages
-----------

.. toctree::

    vm.utils

Submodules
----------

vm.test\_1 module
-----------------

.. automodule:: vm.test_1
    :members:
    :undoc-members:
    :show-inheritance:

vm.test\_2 module
-----------------

.. automodule:: vm.test_2
    :members:
    :undoc-members:
    :show-inheritance:

vm.test\_3 module
-----------------

.. automodule:: vm.test_3
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: vm
    :members:
    :undoc-members:
    :show-inheritance:




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
