import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="vm",
    version="0.0.10",
    author="Markus Grass",
    author_email="markus_grass@web.de",
    description="Open Source Mission Planning Tool",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/m_grass/vm/",
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
    ),
)
